program Hostsman;

uses
  Vcl.Forms,
  fMain in 'fMain.pas' {frmMain},
  uParser in 'uParser.pas',
  uIP in 'uIP.pas',
  uSys in 'uSys.pas',
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
