object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'frmMain'
  ClientHeight = 450
  ClientWidth = 447
  Color = clBtnFace
  Constraints.MinHeight = 220
  Constraints.MinWidth = 380
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  TextHeight = 15
  object DBGrid: TDBGrid
    Left = 0
    Top = 71
    Width = 447
    Height = 379
    Align = alClient
    BorderStyle = bsNone
    DataSource = dsHosts
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    PopupMenu = pmGrid
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = [fsBold]
    OnTitleClick = DBGridTitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'IPAddress'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Hostname'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Comment'
        Width = 200
        Visible = True
      end>
  end
  object pnTop: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 441
    Height = 36
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 435
    object btnSave: TSpeedButton
      Left = 241
      Top = 0
      Width = 42
      Height = 36
      Action = actSave
      Align = alLeft
      Caption = #57605
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe MDL2 Assets'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object DBNavigator: TDBNavigator
      Left = 0
      Top = 0
      Width = 241
      Height = 36
      DataSource = dsHosts
      VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
      Align = alLeft
      Flat = True
      Hints.Strings = (
        'First record'
        'Prior record'
        'Next record'
        'Last record'
        'Insert host'
        'Delete host'
        'Edit host'
        'Post edit'
        'Cancel edit'
        'Refresh'
        'Apply updates'
        'Cancel updates')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = DBNavigatorClick
    end
  end
  object sbFilter: TSearchBox
    AlignWithMargins = True
    Left = 3
    Top = 45
    Width = 441
    Height = 23
    Align = alTop
    BorderStyle = bsNone
    TabOrder = 2
    OnKeyUp = sbFilterKeyUp
    OnInvokeSearch = sbFilterInvokeSearch
    ExplicitWidth = 435
  end
  object mtHosts: TFDMemTable
    BeforePost = mtHostsBeforePost
    AfterPost = mtHostsAfterPost
    AfterDelete = mtHostsAfterDelete
    FieldDefs = <
      item
        Name = 'IPAddress'
        Attributes = [faRequired]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Hostname'
        Attributes = [faRequired]
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Comment'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 400
    Top = 240
  end
  object dsHosts: TDataSource
    DataSet = mtHosts
    Left = 328
    Top = 240
  end
  object ActionList: TActionList
    Left = 120
    Top = 264
    object actSave: TAction
      Category = 'File'
      Caption = '&Save'
      Hint = 'Save hosts'
      ShortCut = 16467
      OnExecute = actSaveExecute
      OnUpdate = UpdateGridAction
    end
    object actQuit: TAction
      Category = 'File'
      Caption = '&Quit'
      Hint = 'Quit'
      ShortCut = 16465
      OnExecute = actQuitExecute
    end
    object actCopyLine: TEditCopy
      Category = 'Edit'
      Caption = 'Copy &Line'
      Hint = 'Copy|Copies the selection and puts it on the Clipboard'
      ImageIndex = 1
      ShortCut = 24643
      OnExecute = actCopyLineExecute
      OnUpdate = UpdateGridAction
    end
    object actViewShowComment: TAction
      Category = 'View'
      Caption = 'Comment Column'
      Checked = True
      OnExecute = actViewShowCommentExecute
      OnUpdate = UpdateGridAction
    end
    object actFind: TAction
      Category = 'Edit'
      Caption = '&Find'
      ShortCut = 16454
      OnExecute = actFindExecute
    end
    object actInsert: TAction
      Category = 'Edit'
      Caption = '&Insert'
      OnExecute = actInsertExecute
      OnUpdate = UpdateGridAction
    end
    object actDelete: TAction
      Category = 'Edit'
      Caption = '&Delete'
      ShortCut = 16430
      OnExecute = actDeleteExecute
      OnUpdate = UpdateGridAction
    end
    object actEdit: TAction
      Category = 'Edit'
      Caption = '&Edit'
      OnExecute = actEditExecute
      OnUpdate = UpdateGridAction
    end
    object actPostChanges: TAction
      Category = 'Edit'
      Caption = '&Post Changes'
      OnExecute = actPostChangesExecute
      OnUpdate = UpdateGridActionInEdit
    end
    object actCancelEdit: TAction
      Category = 'Edit'
      Caption = 'Ca&ncel Edit'
      OnExecute = actCancelEditExecute
      OnUpdate = UpdateGridActionInEdit
    end
    object actViewToolbar: TAction
      Category = 'View'
      Caption = '&Toolbar'
      Checked = True
      OnExecute = actViewToolbarExecute
    end
    object actOpen: TAction
      Category = 'File'
      Caption = '&Open...'
      ShortCut = 16463
      OnExecute = actOpenExecute
      OnUpdate = UpdateGridAction
    end
    object actSaveAs: TAction
      Category = 'File'
      Caption = 'Save &As...'
      OnExecute = actSaveAsExecute
      OnUpdate = UpdateGridAction
    end
  end
  object FDBatchMoveTextWriter: TFDBatchMoveTextWriter
    FileName = 'test.txt'
    DataDef.Fields = <
      item
        FieldName = 'IPAddress'
        DataType = atString
        FieldSize = 20
      end
      item
        FieldName = 'Hostname'
        DataType = atString
        FieldSize = 255
      end
      item
        FieldName = 'Comment'
        DataType = atString
        FieldSize = 255
      end>
    DataDef.Delimiter = #0
    DataDef.Separator = ' '
    DataDef.RecordFormat = rfCustom
    Left = 296
    Top = 360
  end
  object FDBatchMoveDataSetReader: TFDBatchMoveDataSetReader
    DataSet = mtHosts
    Left = 128
    Top = 352
  end
  object FDBMToFile: TFDBatchMove
    Reader = FDBatchMoveDataSetReader
    Writer = FDBatchMoveTextWriter
    Options = [poClearDest, poIdentityInsert, poCreateDest, poSkipUnmatchedDestFields, poUseTransactions]
    Mappings = <>
    LogFileName = 'Data.log'
    OnWriteValue = FDBMToFileWriteValue
    Left = 216
    Top = 296
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrHourGlass
    Left = 224
    Top = 224
  end
  object pmGrid: TPopupMenu
    Left = 48
    Top = 176
    object miCopy: TMenuItem
      Action = actCopyLine
    end
    object miInsert: TMenuItem
      Action = actInsert
    end
    object miDelete: TMenuItem
      Action = actDelete
    end
  end
  object MainMenu: TMainMenu
    Left = 152
    Top = 128
    object mmiFileMenu: TMenuItem
      Caption = '&File'
      object mmiOpenFile: TMenuItem
        Action = actOpen
      end
      object mmiSaveFile: TMenuItem
        Action = actSave
      end
      object mmiSaveAs: TMenuItem
        Action = actSaveAs
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object mmiQuit: TMenuItem
        Action = actQuit
      end
    end
    object mmiEditMenu: TMenuItem
      Caption = '&Edit'
      object mmiCopy: TMenuItem
        Action = actCopyLine
      end
      object mmiInsert: TMenuItem
        Action = actInsert
      end
      object mmiEdit: TMenuItem
        Action = actEdit
      end
      object mmiDelete: TMenuItem
        Action = actDelete
      end
      object mmiPost: TMenuItem
        Action = actPostChanges
      end
      object mmiCancel: TMenuItem
        Action = actCancelEdit
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object mmiFind: TMenuItem
        Action = actFind
      end
    end
    object mmiViewMenu: TMenuItem
      Caption = '&View'
      object mmiToolbar: TMenuItem
        Action = actViewToolbar
      end
      object mmiComment: TMenuItem
        Action = actViewShowComment
      end
    end
  end
  object OpenDialog: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 320
    Top = 160
  end
  object SaveDialog: TSaveDialog
    Options = [ofOverwritePrompt, ofHideReadOnly, ofCreatePrompt, ofEnableSizing]
    Left = 392
    Top = 160
  end
end
