unit fMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, Vcl.DBCtrls, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Buttons, System.Actions,
  Vcl.ActnList, FireDAC.Comp.BatchMove.Text, FireDAC.Comp.BatchMove,
  FireDAC.Comp.BatchMove.DataSet, FireDAC.UI.Intf, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, Vcl.StdActns, Vcl.Menus, Vcl.StdCtrls, Vcl.WinXCtrls;

type
  TfrmMain = class(TForm)
    mtHosts: TFDMemTable;
    dsHosts: TDataSource;
    DBGrid: TDBGrid;
    pnTop: TPanel;
    DBNavigator: TDBNavigator;
    btnSave: TSpeedButton;
    ActionList: TActionList;
    actSave: TAction;
    FDBatchMoveTextWriter: TFDBatchMoveTextWriter;
    FDBatchMoveDataSetReader: TFDBatchMoveDataSetReader;
    FDBMToFile: TFDBatchMove;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    actQuit: TAction;
    pmGrid: TPopupMenu;
    actCopyLine: TEditCopy;
    miCopy: TMenuItem;
    actViewShowComment: TAction;
    sbFilter: TSearchBox;
    actFind: TAction;
    MainMenu: TMainMenu;
    mmiFileMenu: TMenuItem;
    mmiSaveFile: TMenuItem;
    mmiQuit: TMenuItem;
    N1: TMenuItem;
    mmiEditMenu: TMenuItem;
    mmiFind: TMenuItem;
    mmiCopy: TMenuItem;
    N2: TMenuItem;
    actInsert: TAction;
    miInsert: TMenuItem;
    mmiInsert: TMenuItem;
    actDelete: TAction;
    mmiDelete: TMenuItem;
    miDelete: TMenuItem;
    actEdit: TAction;
    mmiEdit: TMenuItem;
    actPostChanges: TAction;
    actCancelEdit: TAction;
    mmiPost: TMenuItem;
    mmiCancel: TMenuItem;
    actViewToolbar: TAction;
    mmiViewMenu: TMenuItem;
    mmiToolbar: TMenuItem;
    mmiComment: TMenuItem;
    actOpen: TAction;
    mmiOpenFile: TMenuItem;
    OpenDialog: TOpenDialog;
    actSaveAs: TAction;
    SaveDialog: TSaveDialog;
    mmiSaveAs: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure DBGridTitleClick(Column: TColumn);
    procedure actSaveExecute(Sender: TObject);
    procedure UpdateGridAction(Sender: TObject);
    procedure FDBMToFileWriteValue(ASender: TObject;
      AItem: TFDBatchMoveMappingItem; var AValue: Variant);
    procedure actQuitExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure mtHostsBeforePost(DataSet: TDataSet);
    procedure mtHostsAfterPost(DataSet: TDataSet);
    procedure mtHostsAfterDelete(DataSet: TDataSet);
    procedure actCopyLineExecute(Sender: TObject);
    procedure actViewShowCommentExecute(Sender: TObject);
    procedure sbFilterInvokeSearch(Sender: TObject);
    procedure actFindExecute(Sender: TObject);
    procedure sbFilterKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure actInsertExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
    procedure actEditExecute(Sender: TObject);
    procedure actPostChangesExecute(Sender: TObject);
    procedure actCancelEditExecute(Sender: TObject);
    procedure UpdateGridActionInEdit(Sender: TObject);
    procedure actViewToolbarExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
  private
    { Private declarations }
    FHostsFile: string;
    FSaved: Boolean;
    procedure SetHostsFile(const Value: string);
    procedure LoadAndParseHostsFile;
    procedure UpdateGridColumnTitleSort(Column: TColumn);
    procedure ApplyFilter;
    procedure ResetFilter;
    function GetModified: Boolean;
    procedure SetModified(const Value: Boolean);
    procedure UpdateCaption;
    procedure SaveHosts;
  public
    { Public declarations }
    property HostsFile: string read FHostsFile write SetHostsFile;
    property Modified: Boolean read GetModified write SetModified;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses uParser, uSys, uIP, Vcl.Clipbrd, System.UITypes, System.StrUtils;

const
  APP_CAPTION = 'EtcHosts';

  // https://en.delphipraxis.net/topic/3322-grid-sort-indicator/
  SORT_ASC = #$25B2;
  SORT_DESC = #$25BC;

procedure TfrmMain.actDeleteExecute(Sender: TObject);
begin
  DBNavigator.BtnClick(nbDelete);
end;

procedure TfrmMain.actEditExecute(Sender: TObject);
begin
  DBNavigator.BtnClick(nbEdit);
end;

procedure TfrmMain.actCancelEditExecute(Sender: TObject);
begin
  DBNavigator.BtnClick(nbCancel);
end;

procedure TfrmMain.actCopyLineExecute(Sender: TObject);
var
  S, Comment: string;
begin
  Comment := mtHosts.FieldByName('Comment').AsString;
  S := format('%s %s', [mtHosts.FieldByName('IPAddress').AsString,
    mtHosts.FieldByName('Hostname').AsString]);
  if Comment.Trim <> '' then
    S := S + ' # ' + Comment;
  if S.Trim = '' then
    Exit;
  Clipboard.AsText := S;
end;

procedure TfrmMain.actFindExecute(Sender: TObject);
begin
  sbFilter.Visible := True;
  sbFilter.SetFocus;
end;

procedure TfrmMain.actInsertExecute(Sender: TObject);
begin
  DBNavigator.BtnClick(nbInsert);
end;

procedure TfrmMain.actOpenExecute(Sender: TObject);
begin
  if Modified and
    (messagedlg('Changes not saved. Are you sure you want to open a file ?',
    TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0, mbNo) = mrNo) then
    Exit;

  if OpenDialog.Execute then
  begin
    mtHosts.EmptyDataSet;
    ResetFilter;
    HostsFile := OpenDialog.FileName;
  end;
end;

procedure TfrmMain.actPostChangesExecute(Sender: TObject);
begin
  DBNavigator.BtnClick(nbPost);
end;

procedure TfrmMain.actQuitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.actSaveAsExecute(Sender: TObject);
begin
  with SaveDialog do
  begin
    FileName := ExtractFileName(Self.HostsFile);
    if Execute then
    begin
      FHostsFile := FileName;
      FDBatchMoveTextWriter.FileName := FileName;
      SaveHosts;
      UpdateCaption;
    end;
  end;
end;

procedure TfrmMain.actSaveExecute(Sender: TObject);
begin
  SaveHosts;
end;

procedure TfrmMain.actViewShowCommentExecute(Sender: TObject);
var
  Col: TColumn;
begin
  for var Item in DBGrid.Columns do
  begin
    Col := Item as TColumn;
    if Col.FieldName = 'Comment' then
      with Sender as TAction do
      begin
        Checked := not Checked;
        Col.Visible := Checked;
        Exit;
      end;
  end;
end;

procedure TfrmMain.actViewToolbarExecute(Sender: TObject);
begin
  with Sender as TAction do
  begin
    Checked := not Checked;
    pnTop.Visible := Checked;
  end;
end;

procedure TfrmMain.ApplyFilter;
var
  S: string;
begin
  with mtHosts do
  begin
    Filtered := false;

    S := Trim(sbFilter.Text);
    if S.Length = 0 then
      Exit;

    Filter := format
      ('IPAddress=''%s'' OR Hostname LIKE ''%%%s%%'' OR Comment LIKE ''%%%s%%''',
      [S, S, S]);

    Filtered := True;
  end;
end;

procedure TfrmMain.UpdateCaption;
begin
  Caption := APP_CAPTION;
  if HostsFile <> '' then
    Caption := format('%s: %s%s', [Caption, ifthen(Modified, '* '), HostsFile]);
end;

procedure TfrmMain.UpdateGridAction(Sender: TObject);
begin
  // The Action is enabled when the Grid is in browse state.
  (Sender as TAction).Enabled := mtHosts.State = dsBrowse;
end;

procedure TfrmMain.UpdateGridActionInEdit(Sender: TObject);
begin
  // The Action is enabled when the Grid row is edited.
  (Sender as TAction).Enabled := (mtHosts.State in [dsEdit, dsInsert]);
end;

procedure TfrmMain.UpdateGridColumnTitleSort(Column: TColumn);
var
  ind: TFDIndex;
  SortSym: string;
begin
  ind := mtHosts.Indexes.FindIndex('By_' + Column.FieldName);
  if ind = nil then
    Exit;

  for var Col in DBGrid.Columns do
    (Col as TColumn).Title.Caption := (Col as TColumn).DisplayName;

  if soDescending in ind.Options then
    SortSym := SORT_DESC
  else
    SortSym := SORT_ASC;

  Column.Title.Caption := Column.DisplayName + '   ' + SortSym;
end;

procedure TfrmMain.DBGridTitleClick(Column: TColumn);
var
  S: string;
  ind: TFDIndex;
begin
  S := 'By_' + Column.FieldName;
  with mtHosts.Indexes do
  begin
    ind := FindIndex(S);
    if ind = nil then
    begin
      ind := Add;
      ind.Name := S;
      ind.Fields := Column.FieldName;
      ind.Active := True;
    end
    else
    begin
      if soDescending in ind.Options then
        ind.Options := ind.Options - [soDescending]
      else
        ind.Options := ind.Options + [soDescending];
    end;
    mtHosts.IndexName := S;
  end;
  UpdateGridColumnTitleSort(Column);
end;

procedure TfrmMain.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
  //
end;

procedure TfrmMain.FDBMToFileWriteValue(ASender: TObject;
  AItem: TFDBatchMoveMappingItem; var AValue: Variant);
var
  C: string;
  IsStrValue: Boolean;
begin
  IsStrValue := ((VarType(AValue) and varTypeMask) = varString);
  if (AItem.SourceField.FieldName = 'Comment') and IsStrValue then
  begin
    C := AValue;
    if (C.Trim <> '') then
      C := '# ' + C;
    AValue := C;
  end;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  Res: Integer;
begin
  if Modified then
  begin
    Res := messagedlg('Changes not saved. Are you sure you want to quit ?',
      TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0, mbNo);
    CanClose := Res = mrYes;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  AFileName: String;
begin
  sbFilter.Visible := false;
  mtHosts.Active := True;

  if FindCmdLineSwitch('f', AFileName) then
    HostsFile := AFileName
  else
    HostsFile := GetSystemHosts;

  Modified := false;
end;

function TfrmMain.GetModified: Boolean;
begin
  Result := mtHosts.Modified or not FSaved;
end;

procedure TfrmMain.LoadAndParseHostsFile;
var
  sl: TStrings;
  parser: THostsParser;
  host: THost;
begin
  sl := TStringList.Create;
  parser := nil;
  try
    sl.LoadFromFile(FHostsFile);
    parser := THostsParser.Create(sl);
    with mtHosts do
    begin
      BeginBatch;
      for host in parser.Hosts do
      begin
        Append;
        FieldByName('IPAddress').AsString := host.IP;
        FieldByName('Hostname').AsString := host.Hostname;
        FieldByName('Comment').AsString := host.Comment;
        Post;
      end;
      EndBatch;
    end;
    Modified := false;
  finally
    sl.Free;
    if Assigned(parser) then
      parser.Free;
  end;
end;

procedure TfrmMain.mtHostsAfterDelete(DataSet: TDataSet);
begin
  Modified := True;
end;

procedure TfrmMain.mtHostsAfterPost(DataSet: TDataSet);
begin
  Modified := True;
end;

procedure TfrmMain.mtHostsBeforePost(DataSet: TDataSet);
var
  IP, Hostname: string;
begin
  // Validate before saving.
  IP := DataSet.FieldByName('IPAddress').AsString;
  if IP.Trim = '' then
    raise Exception.Create('IP Address is required.');
  if not IsIP(IP) then
    raise Exception.CreateFmt('Invalid IP: "%s"', [IP]);

  Hostname := DataSet.FieldByName('Hostname').AsString;
  if Hostname.Trim = '' then
    raise Exception.Create('Hostname is required.');
end;

procedure TfrmMain.ResetFilter;
begin
  sbFilter.Text := '';
  sbFilter.Visible := false;
  mtHosts.Filtered := false;
end;

procedure TfrmMain.SaveHosts;
var
  No: Integer;
  IsFiltered: Boolean;
begin
  DBGrid.Enabled := false;
  No := mtHosts.RecNo;
  IsFiltered := mtHosts.Filtered;
  try
    mtHosts.Filtered := false;
    FDBMToFile.Execute;
    Modified := false;
  finally
    DBGrid.Enabled := True;
    mtHosts.RecNo := No;
    mtHosts.Filtered := IsFiltered;
  end;
end;

procedure TfrmMain.sbFilterInvokeSearch(Sender: TObject);
begin
  ApplyFilter;
  DBGrid.SetFocus;
end;

procedure TfrmMain.sbFilterKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then // Hide SearchBox on Esc key pressing.
  begin
    ResetFilter;
    DBGrid.SetFocus;
  end;
end;

procedure TfrmMain.SetHostsFile(const Value: string);
begin
  FHostsFile := Value;
  if Value <> '' then
    LoadAndParseHostsFile;
  FDBatchMoveTextWriter.FileName := Value;
end;

procedure TfrmMain.SetModified(const Value: Boolean);
begin
  FSaved := not Value;
  UpdateCaption;
end;

end.
