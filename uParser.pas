unit uParser;

interface

uses System.Classes, System.SysUtils, RegularExpressions, uIP;

type

  THost = record
    IP: string;
    Hostname: string;
    Comment: string;
  end;

  THostsArray = array of THost;

  THostsParser = class
  private
    FParsed: Boolean;
    FHosts: THostsArray;
    FStrings: TStrings;
    function GetHosts: THostsArray;
  protected
    procedure Parse; virtual;
  public
    constructor Create(S: TStrings);
    destructor Destroy; override;
    property Parsed: Boolean read FParsed write FParsed;
    property Hosts: THostsArray read GetHosts;
  end;

implementation

{ THostsParser }

constructor THostsParser.Create(S: TStrings);
begin
  FParsed := False;
  FStrings := S;
end;

destructor THostsParser.Destroy;
begin
  inherited;
end;

function THostsParser.GetHosts: THostsArray;
begin
  if not FParsed then
    Parse;

  Result := FHosts;
end;

procedure THostsParser.Parse;
const
  CAP = 10;
var
  I, HostsCount: integer;
  Line, sIp, sComment: string;
  reSkip, reHosts: TRegEx;
  match: TMatch;
  sHost: string;
begin
  HostsCount := 0;
  reSkip := TRegEx.Create('^[ \t]*(#.*)?$');
  reHosts := TRegEx.Create('^[ \t]*([^ \t]+)([^#]+?)([ \t]*#(.+))?$');
  SetLength(FHosts, CAP);
  for I := 0 to FStrings.Count - 1 do
  begin
    Line := FStrings[I];
    if (Trim(Line) <> '') and not reSkip.IsMatch(Line) then
    begin
      match := reHosts.match(Line);
      case match.Groups.Count of
        3, 5:
          begin
            sIp := match.Groups[1].Value;
            if not IsIP(sIP) then
              Continue;
            if match.Groups.Count = 5 then
              sComment := Trim(match.Groups[4].Value)
            else
              sComment := '';
          end;
      else
        Continue;
      end;
      for sHost in match.Groups[2].Value.Split([' ', #09]) do
        if sHost.Trim <> '' then
        begin
          if HostsCount > Length(FHosts) - 1 then
            SetLength(FHosts, Length(FHosts) + CAP);
          with FHosts[HostsCount] do
          begin
            IP := sIp;
            Hostname := sHost.Trim;
            Comment := sComment;
          end;
          Inc(HostsCount);
        end;
    end;
  end;
  SetLength(FHosts, HostsCount);
  FParsed := True;
end;

end.
