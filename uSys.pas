unit uSys;

interface

function GetSystemDir: string;
function GetSystemHosts: string;

implementation

uses System.IOUtils, System.SysUtils;

function GetSystemDir: string;
begin
  { TODO : Replace with Windows api function. }
  Result := GetEnvironmentVariable('WINDIR') + '\system32';
end;

function GetSystemHosts: string;
begin
  Result := GetSystemDir + '\drivers\etc\hosts';
end;

end.
